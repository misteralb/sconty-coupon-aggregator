<?php

require 'helpers.php';
require 'wp.php';

if(isset($_GET['url'])) {
	$url = sanitize($_GET['url']);
	$is_scount_coupon = is_sconty_coupon($url);
	if($is_scount_coupon != 'no') {
		if($url != 'http://sconty.it/') {
		    $coupon = parseCouponData($url);
		    if($coupon) prepareForm($coupon);
		} else {
            // get all sconty mainpage coupon links
		    $links = sconty_crawler();
		    $couponsdata = array();
            // save all coupon links to the links array
		    foreach($links as $link) {
		        $coupondata = parseCouponData($link);
		        array_push($couponsdata, $coupondata);
		    }
            // save data in wp database automatically
            if(is_user_logged_in()) {
                global $current_user;
                get_currentuserinfo();
                if($current_user->roles['0'] == 'administrator') {
                    foreach($couponsdata as $coupon) { multicoupontowp($coupon); }
                } else {
                    echo "<b>You have no authorization to save data into WP!</b>";
                }
            } else {
                echo "<b>You have no authorization to save data into WP!</b>";
            }

		}
	} else {
		echo 'Only Sconty URL should be used!';
	}
} else {
	echo 'Invalid command!';
}

// Parse Coupon Data
function parseCouponData($url) {
	$dom = new DOMDocument;
    libxml_use_internal_errors(true);
    $dom->loadHTMLFile($url);
    $DOMxpath = new DOMXPath($dom);

    $tags = get_meta_tags($url);
    
    // Coupon Title
	foreach ($DOMxpath->query("//meta[@property='og:title']") as $el) {
    	$title = $el->getAttribute("content");
	}

	// Coupon Description
	foreach ($DOMxpath->query("//meta[@property='og:description']") as $el) {
    	$description = $el->getAttribute("content");
	}

    // Sellers Name
    $data = $DOMxpath->query("//span[@class='item']");
    $sellersname = $data->item(0)->textContent;

    // Coupon Price
    $data = $DOMxpath->query("//span[@class='prezzo']");
    $price = $data->item(0)->textContent;
    $price = preg_replace("/[^0-9\,]/", "", $price);

    // Coupon Start & End Date
    $coupon_start_date  = get_current_date(); 
    $coupon_end_date    = coupon_end_date();

    // Coupon Image
    foreach ($DOMxpath->query("//meta[@property='og:image']") as $el) {
    	$image = $el->getAttribute("content");
	}

    $couponcontent = array($title, $image, $price, $description, $sellersname, $coupon_end_date, $coupon_start_date, $url);
    return $couponcontent;
}

// Show Form where the user can modify the data
function prepareForm($couponcontent) { ?>
    
    <div class="row control-group" id="couponcontent">
        <input type="hidden" id="currentdate" name="currentdate" value="<?php echo get_current_date(); ?>">
        <input type="hidden" id="couponenddate" name="couponenddate" value="<?php echo trim($couponcontent['5']); ?>">
        <div class="form-group col-md-8">
            <label for="coupontitle">Coupon Title</label>
            <input type="text" class="form-control" id="coupontitle" name="coupontitle" value="<?php echo "Buono Sconto da " . trim($couponcontent['2']) . "&euro; per " . trim($couponcontent['0']); ?>">
        </div>
        <div class="form-group col-md-4">
            <label for="couponseller">Coupon Seller Name</label>
            <input type="text" class="form-control" id="couponseller" name="couponseller" value="<?php echo trim($couponcontent['4']); ?>">
        </div>
        <div class="form-group col-md-8">
            <label for="couponimage">Coupon Image</label>
            <input type="text" class="form-control" id="couponimage" name="couponimage" value="<?php echo $couponcontent['1']; ?>">
        </div>
        <div class="form-group col-md-4">
            <label for="coupondiscount">Coupon Discount</label>
            <input type="text" class="form-control" id="coupondiscount" name="coupondiscount" value="<?php echo trim($couponcontent['2']); ?>">
        </div>
        <div class="form-group col-md-12">
            <label for="coupondescription">Coupon Description</label>
            <textarea class="form-control" id="coupondescription" name="coupondescription">
            	<?php echo trim($couponcontent['3']); ?>
            </textarea>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <button type="submit" id="createcoupon" class="btn btn-success">Create Coupon</button>
        </div>
    </div>

    <script type="text/javascript">
        $('#coupondescription').summernote({height: 200, minHeight: 200, maxHeight: 300});

        validate();
        $('#coupontitle, #couponimage, #coupondiscount, #coupondescription, #couponseller').change(validate);

        function validate(){
            if ($('#coupontitle').val().length > 0 &&
            	$('#couponimage').val().length > 0 &&
            	$('#coupondiscount').val().length > 0 &&
            	$('#coupondescription').val().length > 0 &&
            	$('#couponseller').val().length > 0) {
                	$("#createcoupon").prop("disabled", false);
            }
            else {
                $("#createcoupon").prop("disabled", true);
            }
        }

        $('#createcoupon').click(function(){

            var url = $('#url').val();
            var currentdate = $('#currentdate').val();
            var enddate = $('#couponenddate').val();

            var coupontitle = $('#coupontitle').val();
            var couponimage = $('#couponimage').val();
            var coupondiscount = $('#coupondiscount').val();
            var coupondescription = $('#coupondescription').val();
            var couponseller = $('#couponseller').val();

            $.ajax({
                type: 'GET',
                url: 'inc/wp.php',
                data: {createcoupon:true, url:url, currentdate:currentdate, coupontitle:coupontitle, couponimage:couponimage, coupondiscount:coupondiscount, coupondescription:coupondescription, couponseller:couponseller, enddate:enddate},
                success:function(result){
                    $('#wpresult').html(result);
                },
                error: function (textStatus, errorThrown) {
                    $('#wpresult').html(errorThrown);
                }
            });

        });
    </script>

    <?php
}