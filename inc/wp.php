<?php

// WP BOOTSTRAP
require '../../wp-blog-header.php';
// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
require '../../wp-admin/includes/image.php';

// Attach Image on Deal
function add_deal_images($post_id, $deal_image) {
	// Download image during parsing
	$image = file_get_contents($deal_image);
	// Wordpress Upload Dirs
	$upload_dir = wp_upload_dir();
	$upload_path = $upload_dir['path'];
	$upload_url = $upload_dir['url'];
	// Image Filename
	$image_name = $post_id . '-' . basename($deal_image);
	// Download File from external server to our server
	$upload_file = $upload_path . "/" . $image_name;
	file_put_contents($upload_file, $image);
	$deal_image = $upload_url . "/" . $image_name;

	// $filename should be the path to a file in the upload directory.
	$filename = $upload_file;
	// The ID of the post this attachment is for.
	$parent_post_id = $post_id;
	// Check the type of file. We'll use this as the 'post_mime_type'.
	$filetype = wp_check_filetype(basename($filename), null);

	// Prepare an array of post data for the attachment.
	$attachment = array(
	    'guid'           => $deal_image, 
	    'post_mime_type' => $filetype['type'],
	    'post_title'     => preg_replace( '/\.[^.]+$/', '', basename($filename)),
	    'post_content'   => '',
	    'post_status'    => 'inherit'
	);

	// Insert the attachment.
	$attach_id = wp_insert_attachment($attachment, $filename, $parent_post_id);

	// Generate the metadata for the attachment, and update the database record.
	$attach_data = wp_generate_attachment_metadata($attach_id, $filename);
	wp_update_attachment_metadata($attach_id, $attach_data);

	$dealImage = array($attach_id, $deal_image);
	return $dealImage;
}

if(isset($_GET['createcoupon'])) {

	function couponsavetowp() {
		// CONSTANTS
		$author_id = "24";
		$deal_category_id = "13";
		$deal_location = "52";
		$deal_gender = "Entrambi";
		$deal_age_groups = array('All Age Ranges');
		

		$url = $_GET['url'];

		$currentdate = $_GET['currentdate'];
		$dealenddate = $_GET['enddate'];

		$coupondescription = htmlentities($_GET['coupondescription']);
		$coupondiscount = $_GET['coupondiscount'];
		$couponimage = $_GET['couponimage'];
		$couponseller = $_GET['couponseller'];

		$coupontitle = htmlentities($_GET['coupontitle']);

		// Save Deal Data to DB
		$page_check = get_page_by_title($coupontitle, OBJECT, 'deals');
		if(!$page_check->ID){
			$post_id = wp_insert_post(array('post_author' => $author_id, 'post_type' => 'deals', 'post_status' => 'publish', 'post_title' => $coupontitle));

			add_post_meta($post_id, 'seller_name', $couponseller);
			add_post_meta($post_id, 'gender', $deal_gender);
			add_post_meta($post_id, 'age_range', $deal_age_groups);

			add_post_meta($post_id, 'affiliate_link', $url);
			add_post_meta($post_id, 'deal_description', $coupondescription);
			add_post_meta($post_id, 'coupon_discount', $coupondiscount);
			
			add_post_meta($post_id, 'deal_start_date', $currentdate);
			add_post_meta($post_id, 'deal_end_date', $dealenddate);

			// Deal Category & Deal Location
			wp_set_post_terms($post_id, (array)$deal_category_id, 'deals_category', true);
			wp_set_post_terms($post_id, (array)$deal_location_id, 'deals_city', true);

			$deal_image_data = add_deal_images($post_id, $couponimage);
			add_post_meta($post_id, 'deal_image', $deal_image_data['1']);

			echo "Coupon <b>\"" . $coupontitle . "\"</b> has been created successfully!<br/>";
		}else{
		    echo "Coupon <b>\"" . $coupontitle . "\"</b> already exists!<br/>";
		}

	}

	if(is_user_logged_in()) {
		global $current_user;
		get_currentuserinfo();
		if($current_user->roles['0'] == 'administrator') couponsavetowp();
	} else {
		echo "<b>You have no authorization to save data into WP!</b>";
	}

}

function multicoupontowp($coupon) {

	// CONSTANTS
	$author_id = "24";
	$deal_category_id = "13";
	$deal_location = "52";
	$deal_gender = "Entrambi";
	$deal_age_groups = array('All Age Ranges');
	
	$url = $coupon['7'];

	$currentdate = $coupon['6'];
	$dealenddate = $coupon['5'];

	$coupontitle = htmlentities($coupon['0']);
	$coupondescription = htmlentities($coupon['3']);
	$coupondiscount = $coupon['2'];
	$couponimage = $coupon['1'];
	$couponseller = $coupon['4'];

	// Save Deal Data to DB
	$page_check = get_page_by_title($coupontitle, OBJECT, 'deals');
	if(!$page_check->ID){
		$post_id = wp_insert_post(array('post_author' => $author_id, 'post_type' => 'deals', 'post_status' => 'publish', 'post_title' => $coupontitle));

		add_post_meta($post_id, 'seller_name', $couponseller);
		add_post_meta($post_id, 'gender', $deal_gender);
		add_post_meta($post_id, 'age_range', $deal_age_groups);

		add_post_meta($post_id, 'affiliate_link', $url);
		add_post_meta($post_id, 'deal_description', $coupondescription);
		add_post_meta($post_id, 'coupon_discount', $coupondiscount);
		
		add_post_meta($post_id, 'deal_start_date', $currentdate);
		add_post_meta($post_id, 'deal_end_date', $dealenddate);

		// Deal Category & Deal Location
		wp_set_post_terms($post_id, (array)$deal_category_id, 'deals_category', true);
		wp_set_post_terms($post_id, (array)$deal_location_id, 'deals_city', true);

		$deal_image_data = add_deal_images($post_id, $couponimage);
		add_post_meta($post_id, 'deal_image', $deal_image_data['1']);

		echo "Coupon: <b>\"" . $coupontitle . "\"</b> has been created successfully!<br/>";
	}else{
	    echo "Coupon: <b>\"" . $coupontitle . "\"</b> already exists!<br/>";
	}
}