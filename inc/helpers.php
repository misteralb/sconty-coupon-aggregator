<?php

function sanitize($input) {
	$input = htmlspecialchars($input);
	return $input;
}

function is_sconty_coupon($url) {
    if (strpos($url, 'sconty') > 0) {
        return 'ok';
    } else {
        return 'no';
    }
}

function get_current_date() {
    $current_date = time();
    $current_date = gmdate('d/m/Y', $current_date);
    return $current_date;
}

function coupon_end_date() {
    $time = get_current_date();
    $deal_end_date = date('d/m/Y', strtotime("+30 days"));
    return $deal_end_date;
}

function sconty_crawler() {
	$url = 'http://sconty.it/';
	$doc = new DOMDocument;
	libxml_use_internal_errors(true);
	$doc->loadHTMLFile($url);

	$links = array();
	$arr = $doc->getElementsByTagName("a");
	foreach($arr as $item) {
		$href = $item->getAttribute("href");
		if(strpos($href, 'SchedaProdotto') !== false) {
		    $text = trim(preg_replace("/[\r\n]+/", " ", $item->nodeValue));
	    	array_push($links, 'http://sconty.it' . $href);
	    }
	}

	$links = array_unique($links);
	return $links;
}