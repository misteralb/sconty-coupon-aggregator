<!DOCTYPE html>
<html lang="it-IT">

<head>
    <meta charset="utf-8">
    <title>Sconty Coupon Aggreagator</title>
    <meta name="viewport" content="width=device-width" />
    <!-- Include CSS Files -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
    <style>
    	.container { padding: 20px; }
    </style>
</head>

<body>

    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="input-group input-group-lg">
                    <input type="text" id="url" class="form-control" placeholder="enter coupon url ...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" id="go" type="submit">Get Coupon Data</button>
                    </span>
                </div>
            </div>
        </div>

        <br/>

        <div class="row">
            <div class="col-lg-12">
                <div id="result"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div id="wpresult"></div>
            </div>
        </div>

    </div>

    <!-- Include JS Files -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            $('#url').focus();
            
            // when user clicks go ...
            $('#go').click(function(){
                var url = $('#url').val();

                if(url) {
	                $.ajax({
	                    type: 'GET',
	                    url: 'inc/sconty.php',
	                    data: {url:url},
	                    success:function(result){
	                        $('#result').html(result);
	                    },
	                    error: function (textStatus, errorThrown) {
	                        $('#result').html(errorThrown);
	                    }
	                });
           		} else {
           			alert('URL field must not be empty!');
           		}

            });

            // when user presses enter key on keyboard ...
            $('#url').keydown(function(event) {
                if (event.which == 13) {
                    $('#go').trigger('click');
                }
            });
         
        });
    </script>
</body>

</html>